package test.com.vrwebview;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Debug;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;


import java.text.Format;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Abhishek on 5/9/2016.
 */
public class MyWebViewClass extends WebView {
    public MainActivity temp;
    private int scrollSpeedY = 0;
    private int scrollSpeedX = 0;

    public android.os.Handler handler1 = new android.os.Handler();

    public MyWebViewClass(Context context) {
        super(context);
    }

    public MyWebViewClass(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyWebViewClass(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setMainActivity(MainActivity temp1){
        temp = temp1;
    }
    @Override
    public void draw( Canvas canvas ) {
        //returns canvas attached to gl texture to draw on
        Canvas glAttachedCanvas = temp.onDrawViewBegin();
        if(glAttachedCanvas != null) {
            //translate canvas to reflect view scrolling
            float xScale = glAttachedCanvas.getWidth() / (float)canvas.getWidth();
            float yScale = glAttachedCanvas.getHeight() / (float)canvas.getHeight();
            glAttachedCanvas.scale(xScale, yScale);
            glAttachedCanvas.translate(-getScrollX(), -getScrollY());
            //draw the view to provided canvas
            super.draw(glAttachedCanvas);
        }
        // notify the canvas is updated
        temp.onDrawViewEnd();
    }

    public void doUpdate(){
        ViewGroup.LayoutParams params = this.getLayoutParams();
        int DEFAULT_TEXTURE_HEIGHT = (int)((float)temp.DEFAULT_TEXTURE_WIDTH/temp.webViewCyinder.getWidthHeightRatio());
        params.width = temp.DEFAULT_TEXTURE_WIDTH;
        params.height = DEFAULT_TEXTURE_HEIGHT;
        this.setLayoutParams(params);
    }

/*
    @Override
    public boolean onGenericMotionEvent(MotionEvent ev){
        if ((ev.getSource() & InputDevice.SOURCE_JOYSTICK) == InputDevice.SOURCE_JOYSTICK && ev.getAction() == MotionEvent.ACTION_MOVE){
            Log.e("WebView", Integer.toString(ev.getActionMasked()));
            scrollSpeedX = Math.round(40 * ev.getAxisValue(0));
            scrollSpeedY = Math.round(40 * ev.getAxisValue(1));
            ScrollWebView(scrollSpeedY);
            ScrollWebViewX(scrollSpeedX);
            return true;
        }
        return super.onGenericMotionEvent(ev);
    }
    public void ScrollWebView(int amountToScroll){
        int scrollPosY = this.getScrollY();
        if (scrollPosY + amountToScroll > 0){
            scrollPosY = scrollPosY + amountToScroll;
        }
        else{
            scrollPosY = 0;
        }
        this.scrollTo(this.getScrollX(), scrollPosY);
    }
    public void ScrollWebViewX(int amountToScroll){
        int scrollPosX = this.getScrollX();
        if (scrollPosX + amountToScroll > 0){
            scrollPosX = scrollPosX + amountToScroll;
        }
        else{
            scrollPosX = 0;
        }
        this.scrollTo(scrollPosX, this.getScrollY());
    }
    public void AlwaysScroll(){
        if (scrollSpeedX != 0){
            ScrollWebViewX(scrollSpeedX);
        }
        if (scrollSpeedY != 0){
            ScrollWebView(scrollSpeedY);
        }
    }

*/
}