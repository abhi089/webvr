package test.com.vrwebview;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;
// Created by Abhishek - VadR


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MainActivity
        extends CardboardActivity implements CardboardView.StereoRenderer {
    private static final String TAG = "VR Web Browser Activity";
    public boolean openStore = false;
    private static final float Z_NEAR = 0.1f;
    private static final float Z_FAR = 10.0f;

    private static final int COORDS_PER_VERTEX = 3;

    private FloatBuffer cylinderVertices;
    private FloatBuffer textureCoords;
    private FloatBuffer grazePointCoordinates;

    private int cylinderProgram;
    private int cylinderPositionParam;
    private int textureCoordParam;
    private int grazePointProgram;
    private int grazePointCoordParam;
    private int grazePointColorParam;
    private int cylinderModelViewProectionParam;
    private int numberOfVertices;
    private int grazePointModelViewProectionParam;
    private float[] grazePoint;
    private float[] modelCube;
    private float[] camera;
    private float[] view;
    private float[] modelViewProjection;
    private float[] modelView;
    private float[] modelPosition;
    private float[] forwardVector;
    private OptionsTab optionsTab;

    private MyWebViewClass mWebView;
    public CutCylinder webViewCyinder;
    private Vibrator vibrator;
    private TextView debugTestView;
    public static int DEFAULT_TEXTURE_WIDTH = 4000;
    public static int DEFAULT_TEXTURE_HEIGHT = 1500;
    private SurfaceTexture mSurfaceTexture;
    private Surface mSurface;
    private Canvas mSurfaceCanvas;
    private int mGlSurfaceTexture;
    private int mTextureWidth = DEFAULT_TEXTURE_WIDTH;
    private int mTextureHeight = DEFAULT_TEXTURE_HEIGHT;
    private float forwardVectorMag = 0.0f;
    private float pitchLimit;
    private String url;
    private String interstitial;
    // Loads the opengl shaders
    private int loadGLShader(int type, int resId) {
        String code = readRawTextFile(resId);
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, code);
        GLES20.glCompileShader(shader);
        // Get the compilation status.
        final int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        // If the compilation failed, delete the shader.
        if (compileStatus[0] == 0) {
            Log.e(TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = 0;
        }
        if (shader == 0) {
            throw new RuntimeException("Error creating shader. "+type);
        }
        return shader;
    }

    // Checks if we've had an error inside of OpenGL ES, and if so what that error is.
    private static void checkGLError(String label) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, label + ": glError " + error);
            throw new RuntimeException(label + ": glError " + error);
        }
    }

    /**
     * Sets the view to our CardboardView and initializes the transformation matrices we will use
     * to render our scene.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent != null){
            url = intent.getStringExtra("url");
            interstitial = intent.getStringExtra("interstitial");
            if(url != null)
                Log.d("shubham",url);
        }
        setContentView(R.layout.common_ui);
        CardboardView cardboardView = (CardboardView) findViewById(R.id.cardboard_view);
        cardboardView.setRenderer(this);
        // disables the default screen asking to put the phone in cardboard; default behaviour is disabled only
        cardboardView.setTransitionViewEnabled(false);
        // disables the VR mode
        cardboardView.setVRModeEnabled(true);
        cardboardView.setElectronicDisplayStabilizationEnabled(true);
        cardboardView.setOnCardboardBackButtonListener(new Runnable() {
            @Override
            public void run() {
                onBackPressed();
            }
        });
        setCardboardView(cardboardView);
//        debugTestView = (TextView) findViewById(R.id.textView);
//        debugTestView.setText("Test 1");
        webViewCyinder = new CutCylinder();
        pitchLimit = (float)Math.atan(webViewCyinder.getHeight() / webViewCyinder.getRadius());
        optionsTab = new OptionsTab(webViewCyinder.getAngleSpanDegrees(), webViewCyinder.getRadius(), webViewCyinder.getHeight());
        mWebView = (MyWebViewClass) findViewById(R.id.web_view);
        ViewGroup.LayoutParams params = mWebView.getLayoutParams();
        DEFAULT_TEXTURE_HEIGHT = (int)((float)DEFAULT_TEXTURE_WIDTH/webViewCyinder.getWidthHeightRatio());
        mTextureWidth = DEFAULT_TEXTURE_WIDTH;
        mTextureHeight = DEFAULT_TEXTURE_HEIGHT;
        params.width = DEFAULT_TEXTURE_WIDTH;
        params.height = DEFAULT_TEXTURE_HEIGHT;
        mWebView.setLayoutParams(params);
        mWebView.setMainActivity(this);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.addJavascriptInterface(new WebViewInterface(), "Android");
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebContentsDebuggingEnabled(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        if(url != null)
            mWebView.loadUrl(url);
        else if(interstitial != null)
            mWebView.loadData(interstitial, "text/html", "UTF-8");
        else
            mWebView.loadUrl("https://www.amazon.in/");
        grazePoint = new float[3];
        modelCube = new float[16];
        camera = new float[16];
        view = new float[16];
        modelViewProjection = new float[16];
        modelView = new float[16];
        modelPosition = new float[]{0.0f, 0.0f, -1.0f};
        forwardVector = new float[3];
        vibrator = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
    }

    public void loadData(){
        final String adData;
        if(url != null){
            try {
                adData = getStringFromFile(url);
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (mWebView == null) {
                            return;
                        }
                        mWebView.loadData(adData, "text/html", "UTF-8");
                        click();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void click(){
        runOnUiThread(new Runnable() {
            public void run() {
                Log.d("shubham", mWebView.getLeft() + ":" + mWebView.getTop());
                final long uMillis = SystemClock.uptimeMillis();
                mWebView.dispatchTouchEvent(MotionEvent.obtain(uMillis, uMillis,
                        MotionEvent.ACTION_DOWN, mWebView.getLeft(), mWebView.getTop(), 0));
                mWebView.dispatchTouchEvent(MotionEvent.obtain(uMillis, uMillis,
                        MotionEvent.ACTION_UP, mWebView.getLeft(), mWebView.getTop(), 0));
            }
        });
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        String URL="https://www.google.co.in/";
        Intent intent=new Intent(this, NotificationIntentService.class);
        intent.putExtra("url", URL);
        startService(intent);

    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "activity paused");
        final String appPackageName = "com.google.android.apps.plus"; // getPackageName() from Context or Activity object
        if (openStore){
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRendererShutdown() {
        Log.i(TAG, "onRendererShutdown");
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        releaseSurface();
        mGlSurfaceTexture = createTexture();
        if (mGlSurfaceTexture > 0) {
            mSurfaceTexture = new SurfaceTexture(mGlSurfaceTexture);
            mSurfaceTexture.setDefaultBufferSize(mTextureWidth, mTextureHeight);
            mSurface = new Surface(mSurfaceTexture);
        }
        Log.i(TAG, "onSurfaceChanged");
    }

    private int createTexture() {
        int[] textures = new int[1];
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glGenTextures(1, textures, 0);
        checkGlError("Texture generate");
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[0]);
        checkGlError("Texture bind");
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
        return textures[0];
    }

    public void releaseSurface() {
        if (mSurface != null) {
            mSurface.release();
        }
        if (mSurfaceTexture != null) {
            mSurfaceTexture.release();
        }
        mSurface = null;
        mSurfaceTexture = null;

    }

    @Override
    public void onSurfaceCreated(EGLConfig config) {
        Log.i(TAG, "onSurfaceCreated");
        GLES20.glClearColor(1.0f, 0.0f, 0.0f, 1.0f); // Red background so text shows up well.
        grazePoint = new float[]{0.0f, 0.0f, -4.0f};
//        grazePoint = new float[]{4.0f, 1.0f, 0.0f, 4.0f, 0, -1.0f, 4.0f, 0, 1.0f, 4.0f, -1.0f, 0};
//        grazePoint = new float[]{0, 1.0f, -4.0f, -1.0f, 0, -4.0f, 1.0f, 0, -4.0f};
        grazePointCoordinates = ByteBuffer.allocateDirect(3 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        grazePointCoordinates.put(grazePoint).position(0);

        numberOfVertices = webViewCyinder.getNumberOfTriangles();
        cylinderVertices = webViewCyinder.getCoords();
        textureCoords = webViewCyinder.getTextureCoords();

        int vertexShader = loadGLShader(GLES20.GL_VERTEX_SHADER, R.raw.light_vertex);
        int passthroughShader = loadGLShader(GLES20.GL_FRAGMENT_SHADER, R.raw.passthrough_fragment);
        int pointVertexShader = loadGLShader(GLES20.GL_VERTEX_SHADER, R.raw.point_vertex);
        int pointFragmentShader = loadGLShader(GLES20.GL_FRAGMENT_SHADER, R.raw.point_fragment);

        cylinderProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(cylinderProgram, vertexShader);
        GLES20.glAttachShader(cylinderProgram, passthroughShader);
        GLES20.glLinkProgram(cylinderProgram);
        GLES20.glUseProgram(cylinderProgram);
        checkGLError("Cube program");

        cylinderPositionParam = GLES20.glGetAttribLocation(cylinderProgram, "a_Position");
        textureCoordParam = GLES20.glGetAttribLocation(cylinderProgram, "a_TexCoordinate");
        cylinderModelViewProectionParam = GLES20.glGetUniformLocation(cylinderProgram, "u_MVP");
        checkGLError("Cube program params");

        // creates a dot in the center of the screen
        grazePointProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(grazePointProgram, pointVertexShader);
        GLES20.glAttachShader(grazePointProgram, pointFragmentShader);
        GLES20.glLinkProgram(grazePointProgram);
        GLES20.glUseProgram(grazePointProgram);

        grazePointCoordParam = GLES20.glGetAttribLocation(grazePointProgram, "a_Position");
        grazePointColorParam = GLES20.glGetUniformLocation(grazePointProgram, "a_color");
        grazePointModelViewProectionParam = GLES20.glGetUniformLocation(grazePointProgram, "u_MVPP");
        Log.e("color point is ", Integer.toString(grazePointCoordParam));
        Log.e("MVP1 point is ", Integer.toString(grazePointModelViewProectionParam));
//        Log.e(TAG, "MVP param is " + Integer.toString(grazePointModelViewProectionParam));
        checkGLError("onSurfaceCreated");

        optionsTab.onCreate(this);
        Matrix.setIdentityM(modelCube, 0);
        Matrix.translateM(modelCube, 0, modelPosition[0], modelPosition[1], modelPosition[2]);
        checkGLError("updateCubePosition");
    }

    private String readRawTextFile(int resId) {
        InputStream inputStream = getResources().openRawResource(resId);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onNewFrame(HeadTransform headTransform) {
        if (checkScrollBottom()){
            ScrollWebView(false, 40);
        }
        if (checkScrollTop()){
            ScrollWebView(true, 40);
        }
        synchronized (this) {
            mSurfaceTexture.updateTexImage();
        }
        Matrix.setLookAtM(camera, 0, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -2.0f, 0.0f, 1.0f, 0.0f);
        headTransform.getForwardVector(forwardVector, 0);
        checkGLError("onReadyToDraw");
        forwardVectorMag = (float)Math.sqrt(forwardVector[0]*forwardVector[0] + forwardVector[1]*forwardVector[1] + forwardVector[2]*forwardVector[2]);
        grazePoint[0] = forwardVector[0]*4.7f;
        grazePoint[1] = forwardVector[1]*4.7f;
        grazePoint[2] = forwardVector[2]*4.7f
        ;
        grazePointCoordinates.put(grazePoint).position(0);
    }

    @Override
    public void onDrawEye(Eye eye) {
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        checkGLError("colorParam");

        // Apply the eye transformation to the camera.
        Matrix.multiplyMM(view, 0, eye.getEyeView(), 0, camera, 0);
        float[] perspective = eye.getPerspective(Z_NEAR, Z_FAR);
        Matrix.multiplyMM(modelView, 0, view, 0, modelCube, 0);
        Matrix.multiplyMM(modelViewProjection, 0, perspective, 0, modelView, 0);
        drawCube();
//        drawPoint();
    }

    @Override
    public void onFinishFrame(Viewport viewport) {
    }

    public void drawCube() {
        GLES20.glUseProgram(cylinderProgram);
        GLES20.glEnableVertexAttribArray(cylinderPositionParam);
        GLES20.glEnableVertexAttribArray(textureCoordParam);
        GLES20.glVertexAttribPointer(
                cylinderPositionParam, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, cylinderVertices);
        GLES20.glVertexAttribPointer(
                textureCoordParam, 2, GLES20.GL_FLOAT, false, 0, textureCoords);
        GLES20.glUniformMatrix4fv(cylinderModelViewProectionParam, 1, false, modelViewProjection, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, numberOfVertices);
        GLES20.glDisableVertexAttribArray(cylinderPositionParam);
        GLES20.glDisableVertexAttribArray(textureCoordParam);
        checkGLError("Drawing cube");

        GLES20.glUseProgram(grazePointProgram);
        GLES20.glEnableVertexAttribArray(grazePointCoordParam);
        GLES20.glVertexAttribPointer(grazePointCoordParam, 3, GLES20.GL_FLOAT, false, 0, grazePointCoordinates);
        GLES20.glUniform4f(grazePointColorParam, 1.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glUniformMatrix4fv(grazePointModelViewProectionParam, 1, false, modelViewProjection, 0);
        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
        GLES20.glDisableVertexAttribArray(grazePointCoordParam);
//        checkGLError("drawing point");


        optionsTab.Draw(modelViewProjection);
    }

    public void drawPoint(){
        GLES20.glUseProgram(grazePointProgram);
        GLES20.glVertexAttribPointer(grazePointCoordParam, 3, GLES20.GL_FLOAT, false, 0, grazePointCoordinates);
        GLES20.glUniformMatrix4fv(grazePointColorParam, 1, false, new float[]{1.0f, 1.0f, 0.0f, 1.0f}, 0);
        GLES20.glUniformMatrix4fv(grazePointModelViewProectionParam, 1, false, modelViewProjection, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);
        checkGLError("drawing point");
    }
    @Override
    public void onCardboardTrigger() {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        Log.e(TAG, "Motion Event called");
        actionOnTouchEvent(ev);
        return true;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event){
        Log.i(TAG, "Controller key presses");
        int keyCode = event.getKeyCode();
        boolean handled = false;
        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD)
                == InputDevice.SOURCE_GAMEPAD) {
            if (event.getRepeatCount() == 0) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BUTTON_A:
                        Log.i(TAG, "Controller key presses is Button A");
                        actionOnControllerEvent(event);
                        handled = true;
                        break;
                    case KeyEvent.KEYCODE_BUTTON_B:
                        finish();
                        handled = true;
                        break;
                    default:
                        break;
                }
            }
            if (handled) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    public Canvas onDrawViewBegin() {
        mSurfaceCanvas = null;
        if (mSurface != null) {
            try {
                mSurfaceCanvas = mSurface.lockCanvas(null);
            } catch (Exception e) {
                Log.e(TAG, "error while rendering view to gl: " + e);
            }
        }
        return mSurfaceCanvas;
    }

    public void onDrawViewEnd() {
        if (mSurfaceCanvas != null) {
            mSurface.unlockCanvasAndPost(mSurfaceCanvas);
        }
        mSurfaceCanvas = null;
    }

    public void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + GLUtils.getEGLErrorString(error));
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void actionOnTouchEvent(MotionEvent ev){
        float pitchAngle = (float)Math.atan2(forwardVector[1], -forwardVector[2]);
        float yawAngle = (float)Math.atan2(forwardVector[0], -forwardVector[2]);
        float radius = webViewCyinder.getRadius();
        float height = webViewCyinder.getHeight();
        float yawAngleLimits = webViewCyinder.getAngleLimit();
        float percentageYaw = (yawAngle + yawAngleLimits) / yawAngleLimits/2;
        if (pitchAngle < -pitchLimit && ev.getAction() == 0){
            if (percentageYaw > 0 && percentageYaw < 0.25f){
                changeZoom(false);
            }
            if (percentageYaw > 0.25f && percentageYaw < 0.50f){
                if (mWebView.canGoBack()){
                    mWebView.goBack();
                }
            }
            if (percentageYaw > 0.50f && percentageYaw < 0.75f){
                finish();
            }
            if (percentageYaw > 0.75f && percentageYaw < 1.0f){
                changeZoom(true);
            }
        }
        else if(percentageYaw > 0 && percentageYaw < 1.0f && pitchAngle < pitchLimit){
            float heightLookedAt = radius*(float)Math.tan(pitchAngle);
            float clickPoints[] = new float[]{(yawAngle+yawAngleLimits)/2/yawAngleLimits,1.0f -(heightLookedAt+ height)/2/height};
            MotionEvent motionEvent = MotionEvent.obtain(
                    ev.getDownTime(),
                    ev.getEventTime(),
                    ev.getAction(),
                    clickPoints[0]*DEFAULT_TEXTURE_WIDTH,
                    clickPoints[1]*DEFAULT_TEXTURE_HEIGHT,
                    0
            );
            mWebView.dispatchTouchEvent(motionEvent);
            if (ev.getAction() == 0){
//            this.evaluateJavascript("javascript:console.log('yeah it worked');", null);
                mWebView.evaluateJavascript("javascript:console.log(\"script running on click\"); setTimeout(function(){var xtemp = document.getElementsByClassName('id-purchase-sign-in-container'); if (xtemp.length ==0){ console.log('nothing found'); } else{Android.setPlaystoreCall();  xChild = xtemp[1].childNodes[1].childNodes; console.log(xtemp[1].childNodes); xChild[1].innerHTML = 'Thank you for your interest'; xChild[3].innerHTML = 'You will be redirected to playstore when your VR app closes'; }; xtemp[1].childNodes[3].innerHTML = '<br>'}, 1500);", null);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void actionOnControllerEvent(KeyEvent ev){
        Log.i(TAG, "Controller event function called");

//PREVIOUS CALCULATION OF PITCH AND YAW
//        float pitchAngle = (float)Math.asin((forwardVector[1]/(float)Math.sqrt(forwardVector[1] * forwardVector[1] + forwardVector[2] * forwardVector[2])));
//        float yawAngle = (float)Math.asin((forwardVector[0]/(float)Math.sqrt(forwardVector[0] * forwardVector[0] + forwardVector[2] * forwardVector[2])));

       float pitchAngle = (float)Math.atan2(forwardVector[1], -forwardVector[2]);
        float yawAngle = (float)Math.atan2(forwardVector[0], -forwardVector[2]);
        float radius = webViewCyinder.getRadius();
        float height = webViewCyinder.getHeight();
        float yawAngleLimits = webViewCyinder.getAngleLimit();
        float percentageYaw = (yawAngle + yawAngleLimits) / yawAngleLimits/2;
        if (pitchAngle < -pitchLimit && ev.getAction() == 0){
            if (percentageYaw > 0 && percentageYaw < 0.25f){
                Log.i(TAG, "Changing zoom called");
                changeZoom(false);
            }
            if (percentageYaw > 0.25f && percentageYaw < 0.50f){
                if (mWebView.canGoBack()){
                    mWebView.goBack();
                }
            }
            if (percentageYaw > 0.50f && percentageYaw < 0.75f){
                finish();
            }
            if (percentageYaw > 0.75f && percentageYaw < 1.0f){
                changeZoom(true);
            }
        }
        else if(percentageYaw > 0 && percentageYaw < 1.0f && pitchAngle < pitchLimit){
            float heightLookedAt = radius*(float)Math.tan(pitchAngle);
            float clickPoints[] = new float[]{(yawAngle+yawAngleLimits)/2/yawAngleLimits,1.0f -(heightLookedAt+ height)/2/height};
            MotionEvent motionEvent = MotionEvent.obtain(
                    ev.getDownTime(),
                    ev.getEventTime(),
                    ev.getAction(),
                    clickPoints[0]*DEFAULT_TEXTURE_WIDTH,
                    clickPoints[1]*DEFAULT_TEXTURE_HEIGHT,
                    0
            );
            mWebView.dispatchTouchEvent(motionEvent);
            if (ev.getAction() == 0){
//            this.evaluateJavascript("javascript:console.log('yeah it worked');", null);
                mWebView.evaluateJavascript("javascript:console.log(\"script running on click\"); setTimeout(function(){var xtemp = document.getElementsByClassName('id-purchase-sign-in-container'); if (xtemp.length ==0){ console.log('nothing found'); } else{Android.setPlaystoreCall();  xChild = xtemp[1].childNodes[1].childNodes; console.log(xtemp[1].childNodes); xChild[1].innerHTML = 'Thank you for your interest'; xChild[3].innerHTML = 'You will be redirected to playstore when your VR app closes'; }; xtemp[1].childNodes[3].innerHTML = '<br>'}, 1500);", null);
            }
        }
    }
    public boolean checkScrollBottom(){
        float pitchAngle = (float)Math.atan2(forwardVector[1], -forwardVector[2]);
        float yawAngle = (float)Math.atan2(forwardVector[0], -forwardVector[2]);
        float radius = webViewCyinder.getRadius();
        float height = webViewCyinder.getHeight();
        float edgeHeight = webViewCyinder.getEdgeHeight();
        float minPitch = (float)Math.atan2((height-edgeHeight), radius);
        float maxPitch = (float)Math.atan2(height, radius);
        boolean rightPitch = (pitchAngle < 0) && (Math.abs(pitchAngle) > minPitch) && (Math.abs(pitchAngle) < maxPitch);
        float yawAngleLimits = webViewCyinder.getAngleLimit();
        boolean rightYaw = (yawAngle < yawAngleLimits) && (yawAngle > - yawAngleLimits);
        return rightPitch && rightYaw;
    }

    public boolean checkScrollTop(){
        float pitchAngle = (float)Math.atan2(forwardVector[1], -forwardVector[2]);
        float yawAngle = (float)Math.atan2(forwardVector[0], -forwardVector[2]);
        float radius = webViewCyinder.getRadius();
        float height = webViewCyinder.getHeight();
        float edgeHeight = webViewCyinder.getEdgeHeight();
        float minPitch = (float)Math.atan2((height - edgeHeight), radius);
        float maxPitch = (float)Math.atan2(height,radius);
        boolean rightPitch = (pitchAngle > 0) && (Math.abs(pitchAngle) > minPitch) && (Math.abs(pitchAngle) < maxPitch);
        float yawAngleLimits = webViewCyinder.getAngleLimit();
        boolean rightYaw = (yawAngle < yawAngleLimits) && (yawAngle > - yawAngleLimits);
        return rightPitch && rightYaw;
    }

    public void ScrollWebView(boolean up, int amountToScroll){
        int scrollPosY = mWebView.getScrollY();
        if (up){
            if (scrollPosY > amountToScroll) {
                scrollPosY -= amountToScroll;
            }
            else {
                scrollPosY = 0;
            }
        }
        else {
            scrollPosY += amountToScroll;
        }
        mWebView.scrollTo(mWebView.getScrollX(), scrollPosY);
    }

    public float[] pointsClicked(){
        float radius = webViewCyinder.getRadius();
        float height = webViewCyinder.getHeight();
        float pitchAngle = (float)Math.asin((forwardVector[1]/forwardVectorMag));
        float yawAngle = (float)Math.asin((forwardVector[0]/forwardVectorMag));
        float yawAngleLimits = webViewCyinder.getAngleLimit();
        float maxPitch = (float)Math.asin((height)/Math.sqrt((height)*(height) + radius*radius));
        if ((pitchAngle < maxPitch && pitchAngle > -maxPitch) && (yawAngle < yawAngleLimits && yawAngle > -yawAngleLimits)){
            float heightLookedAt = radius*(float)Math.tan(pitchAngle);
            return new float[]{(yawAngle+yawAngleLimits)/2/yawAngleLimits,1.0f -(heightLookedAt+ height)/2/height};
        }
        return new float[] {-1.0f, -1.0f};
    }

    private void changeZoom(boolean zoomIn ){
        int zoomValue = 2;
        if (zoomIn){
            webViewCyinder.changeZoom(2);
        }
        else {
            webViewCyinder.changeZoom(-2);
        }
        numberOfVertices = webViewCyinder.getNumberOfTriangles();
        cylinderVertices = webViewCyinder.getCoords();
        textureCoords = webViewCyinder.getTextureCoords();
        DEFAULT_TEXTURE_HEIGHT = (int)((float)DEFAULT_TEXTURE_WIDTH/webViewCyinder.getWidthHeightRatio());
        mTextureHeight = DEFAULT_TEXTURE_HEIGHT;
        mWebView.handler1.post(new Runnable() {
            @Override
            public void run() {
                mWebView.doUpdate();
            }
        });
        optionsTab.setArrays(webViewCyinder.getAngleSpanDegrees(), webViewCyinder.getRadius(), webViewCyinder.getHeight());
    }

    public void changeWebViewHeight(){
        DEFAULT_TEXTURE_HEIGHT = (int)((float)DEFAULT_TEXTURE_WIDTH/webViewCyinder.getWidthHeightRatio());
        mTextureHeight = DEFAULT_TEXTURE_HEIGHT;
        mWebView.handler1.post(new Runnable() {
            @Override
            public void run() {
                mWebView.doUpdate();
            }
        });
    }

    class WebViewInterface{
        @JavascriptInterface
        public void setPlaystoreCall(){
            Log.e("Playstore function" , "function called");
            openStore = true;
        }
    }
}
