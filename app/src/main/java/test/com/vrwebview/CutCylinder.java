package test.com.vrwebview;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/*
 * Created by Abhishek on 5/6/2016.
 */
public class CutCylinder {
    private float radius = 6.4f;
    private float[] center = {0,0,0};
    private int zoom = 0;
    private int offset_n = 9; // suggested value is offset = 11 and zoom = 0
    private float angle_per_sqaure = 5.0f;
    private float cylinder_height = 4.8f;
    private float edge_height = 1.2f;
    private float cylinder_vertices[];
    private float texture_vertices[];
    private int rectangle_count;
    public CutCylinder(){
        drayArrays();
    }

    private void drayArrays(){
        rectangle_count = (int)((180-2*(angle_per_sqaure*(offset_n-zoom)))/angle_per_sqaure);
        cylinder_vertices = new float[18*rectangle_count];
        float angle_used = (offset_n-zoom)*angle_per_sqaure*22/7/180;
        float sine_left = (float)Math.sin((double) angle_used);
        float cos_left = (float)Math.cos((double) angle_used);;
        float sine_right = 0;
        float cos_right = 0;
        float texture_width = 1.0f/rectangle_count;
        texture_vertices = new float[12*rectangle_count];
        for (int k =1; k<=rectangle_count; k++){
            angle_used = (k+(offset_n - zoom))*angle_per_sqaure*22/7/180;
            sine_right = (float)Math.sin((double) angle_used);
            cos_right = (float)Math.cos((double) angle_used);
            cylinder_vertices[(k-1)*18+0] = center[0] - radius*cos_left;
            cylinder_vertices[(k-1)*18+1] = center[1] + cylinder_height;
            cylinder_vertices[(k-1)*18+2] = center[2] - radius*sine_left;
            cylinder_vertices[(k-1)*18+3] = center[0] - radius*cos_left;
            cylinder_vertices[(k-1)*18+4] = center[1] - cylinder_height;
            cylinder_vertices[(k-1)*18+5] = center[2] - radius*sine_left;
            cylinder_vertices[(k-1)*18+6] = center[0] - radius*cos_right;
            cylinder_vertices[(k-1)*18+7] = center[1] - cylinder_height;
            cylinder_vertices[(k-1)*18+8] = center[2] - radius*sine_right;
            cylinder_vertices[(k-1)*18+9] = center[0] - radius*cos_left;
            cylinder_vertices[(k-1)*18+10] = center[1] + cylinder_height;
            cylinder_vertices[(k-1)*18+11] = center[2] - radius*sine_left;
            cylinder_vertices[(k-1)*18+12] = center[0] - radius*cos_right;
            cylinder_vertices[(k-1)*18+13] = center[1] - cylinder_height;
            cylinder_vertices[(k-1)*18+14] = center[2] - radius*sine_right;
            cylinder_vertices[(k-1)*18+15] = center[0] - radius*cos_right;
            cylinder_vertices[(k-1)*18+16] = center[1] + cylinder_height;
            cylinder_vertices[(k-1)*18+17] = center[2] - radius*sine_right;
            texture_vertices[(k-1)*12+0] = (k-1)*texture_width;
            texture_vertices[(k-1)*12+1] = 0;
            texture_vertices[(k-1)*12+2] = (k-1)*texture_width;
            texture_vertices[(k-1)*12+3] = 1;
            texture_vertices[(k-1)*12+4] = k*texture_width;
            texture_vertices[(k-1)*12+5] = 1;
            texture_vertices[(k-1)*12+6] = (k-1)*texture_width;
            texture_vertices[(k-1)*12+7] = 0;
            texture_vertices[(k-1)*12+8] = k*texture_width;
            texture_vertices[(k-1)*12+9] = 1;
            texture_vertices[(k-1)*12+10] = k*texture_width;
            texture_vertices[(k-1)*12+11] = 0;
            sine_left = sine_right;
            cos_left = cos_right;
        }
    }

    public FloatBuffer getCoords(){
        FloatBuffer temp2 =ByteBuffer.allocateDirect(cylinder_vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        temp2.put(cylinder_vertices);
        temp2.position(0);
        return  temp2;
    }

    public FloatBuffer getTextureCoords(){
        FloatBuffer temp2 =ByteBuffer.allocateDirect(texture_vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        temp2.put(texture_vertices);
        temp2.position(0);
        return  temp2;
    }

    public int getNumberOfTriangles(){
        return rectangle_count*6;
    }

    public float getRadius(){
        return radius;
    }

    public float getHeight(){
        return cylinder_height;
    }

    public float getEdgeHeight(){
        return edge_height;
    }

    public float getAngleLimit(){
        return (90.0f - angle_per_sqaure * (offset_n - zoom))*22/7/180;
    }
    public float getAngleSpanDegrees(){
        return 2 * (90.0f - angle_per_sqaure * (offset_n - zoom));
    }

    public float getWidthHeightRatio(){
        return radius*22/7*(90-angle_per_sqaure*(offset_n-zoom))/90/2/cylinder_height;
    }

    public void changeZoom(int zoomLevel){
        int zoomTemp =  zoom + zoomLevel;
        if ((90 - angle_per_sqaure * (offset_n - zoomTemp)) >= 30 && (90 - angle_per_sqaure * (offset_n - zoomTemp)) <= 90){
            zoom += zoomLevel;
            drayArrays();
        }
    }
}