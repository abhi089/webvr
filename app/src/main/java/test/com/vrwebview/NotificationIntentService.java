package test.com.vrwebview;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Abhishek on 20-06-2016.
 */
public class NotificationIntentService extends IntentService {

    private int UniqueID = 3784950;

    public NotificationIntentService() {
        super("NotificationIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Thread.sleep(10000);
            String URL = intent.getExtras().getString("url");
            Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
            String body= "I am Abhishek !";
            String title="Hello";
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.drawable.ic_settings_48dp);
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(body);
            TaskStackBuilder stackBuilder=TaskStackBuilder.create(this);
            stackBuilder.addNextIntent(intent1);
            PendingIntent resultPendingIntent=stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder.setAutoCancel(true);
            mNotificationManager.notify(UniqueID, mBuilder.build());

        } catch (InterruptedException e) {
            // Restore interrupt status.
            Thread.currentThread().interrupt();
        }
    }
}
