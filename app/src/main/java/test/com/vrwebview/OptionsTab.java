package test.com.vrwebview;

/**
 * Created by shubham on 03-06-2016.
 */
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Abhishek on 5/14/2016.
 */
public class OptionsTab {
    private int buttonTexture;
    private FloatBuffer buttonVertices;
    private FloatBuffer buttonTextureVertices;
    private int buttonProgram;
    private int buttonVerticeParam;
    private int buttonTextureVerticeParam;
    private int buttonMVPParam;
    private int buttonColorParam;
    private final String vertexShaderCode =
            "uniform mat4 buttonMVPMatrix;" +
                    "attribute vec2 a_TextureCoordinates;" +
                    "varying vec2 v_TextureCoordinates;" +
                    "attribute vec4 vButtonPosition;" +
                    "uniform vec4 button_color;" +
                    "varying vec4 v_color;" +
                    "void main() {" +
                    "v_TextureCoordinates = a_TextureCoordinates;" +
                    "  gl_Position = buttonMVPMatrix * vButtonPosition;" +
                    "  v_color = button_color;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform sampler2D u_TextureUnit;" +
                    "varying vec2 v_TextureCoordinates;" +
                    "varying vec4 v_color;" +
                    "void main() {" +
                    "  gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates); " +
                    "}";
    //texture2D(u_TextureUnit, v_TextureCoordinates);
    // v_color;
    private int buttonTextureLocation;

    public OptionsTab(float angleToBeShown, float radiusOfCylinder, float heightOfCylinder){
        setArrays(angleToBeShown, radiusOfCylinder, heightOfCylinder);
    }


    public void setArrays(float angleToBeShown, float radiusOfCylinder, float heightOfCylinder){
        float vertices[] = new float[54];
        float initial_angle = (180 - angleToBeShown)/2*22/7/180;
        float anglePerButton = angleToBeShown/16*22/7/180;
        float sinAngle1 = (float)Math.sin(initial_angle);
        float cosAngle1 = (float)Math.cos(initial_angle);
        float sinAngle2 = (float)Math.sin(initial_angle + anglePerButton);
        float cosAngle2 = (float)Math.cos(initial_angle + anglePerButton);
        float sinAngle3 = (float)Math.sin(initial_angle + 2 * anglePerButton);
        float cosAngle3 = (float)Math.cos(initial_angle + 2 * anglePerButton);
        float sinAngle4 = (float)Math.sin(initial_angle + 3 * anglePerButton);
        float cosAngle4 = (float)Math.cos(initial_angle + 3 * anglePerButton);
        float sinAngle5 = (float)Math.sin(initial_angle + 4 * anglePerButton);
        float cosAngle5 = (float)Math.cos(initial_angle + 4 * anglePerButton);
        float sinAngle6 = (float)Math.sin(initial_angle + 5 * anglePerButton);
        float cosAngle6 = (float)Math.cos(initial_angle + 5 * anglePerButton);
        float sinAngle7 = (float)Math.sin(initial_angle + 6 * anglePerButton);
        float cosAngle7 = (float)Math.cos(initial_angle + 6 * anglePerButton);
        float sinAngle8 = (float)Math.sin(initial_angle + 7 * anglePerButton);
        float cosAngle8 = (float)Math.cos(initial_angle + 7 * anglePerButton);
        float dimension1 = radiusOfCylinder * 0.9f;
        float dimension2 = radiusOfCylinder * 0.9f * (float)Math.sqrt(17.0f/ 16.0f);
        float dimension3 = radiusOfCylinder * 0.9f * (float)Math.sqrt(5.0f / 4.0f);
        float dimension4 = radiusOfCylinder * 0.9f * 1.25f;
        float dimension5 = radiusOfCylinder * 0.9f * (float)Math.sqrt(2);
        vertices[0] = 0;
        vertices[1] = - heightOfCylinder;
        vertices[2] = 0;
        vertices[3] = dimension1 * cosAngle1;
        vertices[4] = - heightOfCylinder;
        vertices[5] = - dimension1 * sinAngle1;
        vertices[6] = dimension2 * cosAngle2;
        vertices[7] = - heightOfCylinder;
        vertices[8] = - dimension2 * sinAngle2;
        vertices[9] = dimension3 * cosAngle3;
        vertices[10] = - heightOfCylinder;
        vertices[11] = - dimension3 * sinAngle3;
        vertices[12] =  dimension4 * cosAngle4;
        vertices[13] = - heightOfCylinder;
        vertices[14] = - dimension4 * sinAngle4;
        vertices[15] = dimension5 * cosAngle5;
        vertices[16] = - heightOfCylinder;
        vertices[17] = - dimension5 * sinAngle5;
        vertices[18] = dimension4 * cosAngle6;
        vertices[19] = - heightOfCylinder;
        vertices[20] = - dimension4 * sinAngle6;
        vertices[21] = dimension3 * cosAngle7;
        vertices[22] = - heightOfCylinder;
        vertices[23] = - dimension3 * sinAngle7;
        vertices[24] = dimension2 * cosAngle8;
        vertices[25] = - heightOfCylinder;
        vertices[26] = - dimension2 * sinAngle8;
        vertices[27] = - 0.0f;
        vertices[28] = - heightOfCylinder;
        vertices[29] = - dimension1;
        vertices[30] = - dimension2 * cosAngle8;
        vertices[31] = - heightOfCylinder;
        vertices[32] = - dimension2 * sinAngle8;
        vertices[33] = - dimension3 * cosAngle7;
        vertices[34] = - heightOfCylinder;
        vertices[35] = - dimension3 * sinAngle7;
        vertices[36] = - dimension4 * cosAngle6;
        vertices[37] = - heightOfCylinder;
        vertices[38] = - dimension4 * sinAngle6;
        vertices[39] = - dimension5 * cosAngle5;
        vertices[40] = - heightOfCylinder;
        vertices[41] = - dimension5 * sinAngle5;
        vertices[42] = - dimension4 * cosAngle4;
        vertices[43] = - heightOfCylinder;
        vertices[44] = - dimension4 * sinAngle4;
        vertices[45] = - dimension3 * cosAngle3;
        vertices[46] = - heightOfCylinder;
        vertices[47] = - dimension3 * sinAngle3;
        vertices[48] = - dimension2 * cosAngle2;
        vertices[49] = - heightOfCylinder;
        vertices[50] = - dimension2 * sinAngle2;
        vertices[51] = - dimension1 * cosAngle1;
        vertices[52] = - heightOfCylinder;
        vertices[53] = - dimension1 * sinAngle1;
        buttonVertices = ByteBuffer.allocateDirect(54 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        buttonVertices.put(vertices).position(0);
        float textureVertices[] = new float[36];
        textureVertices[0] = 0.5f;
        textureVertices[1] = 1.0f;
        textureVertices[2] = 1.0f;
        textureVertices[3] = 1.0f;
        textureVertices[4] = 1.0f;
        textureVertices[5] = 0.75f;
        textureVertices[6] = 1.0f;
        textureVertices[7] = 0.50f;
        textureVertices[8] = 1.0f;
        textureVertices[9] = 0.25f;
        textureVertices[10] = 1.0f;
        textureVertices[11] = 0.0f;
        textureVertices[12] = 0.875f;
        textureVertices[13] = 0.0f;
        textureVertices[14] = 0.75f;
        textureVertices[15] = 0.0f;
        textureVertices[16] = 0.625f;
        textureVertices[17] = 0.0f;
        textureVertices[18] = 0.5f;
        textureVertices[19] = 0.0f;
        textureVertices[20] = 0.375f;
        textureVertices[21] = 0.0f;
        textureVertices[22] = 0.25f;
        textureVertices[23] = 0.0f;
        textureVertices[24] = 0.125f;
        textureVertices[25] = 0.0f;
        textureVertices[26] = 0.0f;
        textureVertices[27] = 0.0f;
        textureVertices[28] = 0.0f;
        textureVertices[29] = 0.25f;
        textureVertices[30] = 0.0f;
        textureVertices[31] = 0.5f;
        textureVertices[32] = 0.0f;
        textureVertices[33] = 0.75f;
        textureVertices[34] = 0.0f;
        textureVertices[35] = 1.0f;
        buttonTextureVertices = ByteBuffer.allocateDirect(36*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        buttonTextureVertices.put(textureVertices).position(0);
    }

    public void onCreate(Context context){
        buttonProgram = GLES20.glCreateProgram();
        int vertexShader = OpenGLHelper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = OpenGLHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
        GLES20.glAttachShader(buttonProgram, vertexShader);
        GLES20.glAttachShader(buttonProgram, fragmentShader);
        GLES20.glLinkProgram(buttonProgram);
        GLES20.glUseProgram(buttonProgram);
        buttonTexture = OpenGLHelper.loadTexture(context, R.raw.webview_lower);
        buttonVerticeParam = GLES20.glGetAttribLocation(buttonProgram, "vButtonPosition");
        buttonTextureVerticeParam = GLES20.glGetAttribLocation(buttonProgram, "a_TextureCoordinates");
        buttonTextureLocation = GLES20.glGetUniformLocation(buttonProgram, "u_TextureUnit");
        buttonMVPParam = GLES20.glGetUniformLocation(buttonProgram, "buttonMVPMatrix");
        buttonColorParam = GLES20.glGetUniformLocation(buttonProgram, "button_color");
        Log.e("buttonVerticeParam", Integer.toString(buttonVerticeParam));
        Log.e("buttonTextureVParam", Integer.toString(buttonTextureVerticeParam));
        Log.e("buttonTextureLocation", Integer.toString(buttonTextureLocation));
        Log.e("buttonMVPParam", Integer.toString(buttonMVPParam));
        Log.e("buttonColorParam", Integer.toString(buttonColorParam));
    }

    public void Draw(float[] buttonMVP){
        GLES20.glUseProgram(buttonProgram);
        GLES20.glEnableVertexAttribArray(buttonVerticeParam);
        GLES20.glEnableVertexAttribArray(buttonTextureVerticeParam);
        GLES20.glVertexAttribPointer(buttonVerticeParam, 3, GLES20.GL_FLOAT, false, 0, buttonVertices);
        GLES20.glVertexAttribPointer(buttonTextureVerticeParam, 2, GLES20.GL_FLOAT, false, 0, buttonTextureVertices);
        GLES20.glUniformMatrix4fv(buttonMVPParam, 1, false, buttonMVP, 0);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, buttonTexture);
        GLES20.glUniform1i(buttonTextureLocation, 0);
        GLES20.glUniform4f(buttonColorParam, 1.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 18);
        GLES20.glDisableVertexAttribArray(buttonVerticeParam);
        GLES20.glDisableVertexAttribArray(buttonTextureVerticeParam);
    }
}